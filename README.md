# Collection of `.gitlab-ci.yml` templates

This is a collection of [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/README.html) file templates.

Files can be included in your `.gitlab-ci.yml` using the `include` keyword.
For example, to use the `Molecule.gitlab-ci.yml` file, add the following lines to your `.gitlab-ci.yml`:

```yaml
include:
  - remote: "https://gitlab.esss.lu.se/ics-infrastructure/gitlab-ci-yml/raw/master/Molecule.gitlab-ci.yml"
```

See https://docs.gitlab.com/ee/ci/yaml/#include for more information.

## Available templates

### `Container-Scanning.gitlab-ci.yml`

Template to scan containers for vulnerability.
Based on https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml

### `Docker.gitlab-ci.yml`

Template to build and push docker images to GitLab registry.
This template makes some assumptions.

- Every push builds a new image with the git tag or branch name as tag
- When pushing a tag, "latest" is applied to that tag. This assumes you don't push "older tag".
  latest always points to the latest pushed tag.

If you need a more complex or different workflow, create your own .gitlab-ci.yml, or a a new template.

### `Molecule.gitlab-ci.yml`

Template to run `molecule test`. Used by all [ics-ansible-galaxy](https://gitlab.esss.lu.se/ics-ansible-galaxy) repositories.

### `ArtifactoryPyPI.gitlab-ci.yml`

Defines a `release-pypi` job (part of the release stage) to upload a Python package to Artifactory PyPI repository
using twine.
The job is only executed on tags.

The following variables should be defined:

- TWINE_REPOSITORY_URL
- TWINE_USERNAME
- TWINE_PASSWORD

This is usually done at the group level.

### `Ioc.gitlab-ci.yml`

Template to deploy an IOC from the GitLab [IOC](https://gitlab.esss.lu.se/ioc) group.

### `Ioc-test.gitlab-ci.yml`

Template used for projects in the [IOC Test](https://gitlab.esss.lu.se/ioc-test) group.
This group is used for testing IOC Factory.

### `CondaBuild.gitlab-ci.yml`

Template to build a conda package and uplaod it to artifactory.
By default, the package is uploaded to the `ics-conda-forge` channel. This can be overriden using the variable `ARTIFACTORY_CONDA_CHANNEL`.
Only linux-64 is currently supported.

The following variables should be defined:

- ARTIFACTORY_URL
- ARTIFACTORY_API_KEY

### `E3CondaBuild.gitlab-ci.yml`

Template to build a E3 conda package and uplaod it to artifactory.
ppc64 cross-compilation (for ifc14xx) is enabled by default (should be skipped in meta.yml if required).

### `E3ModuleBuild.gitlab-ci.yml`

Template to build a E3 module on NFS.
The GitLab runner shall be properly configured to mount the expected NFS share.

### `ifc14xxCondaBuild.gitlab-ci.yml`

Template to build a conda package for ifc14xx using cross-compilation and upload it to artifactory.

### `PhoebusOpis.gitlab-ci.yml`

Template to deploy Phoebus OPIs.
This is for testing right now and is limited to the lab inventory.
The following variables are required:

- TOWER_OAUTH_TOKEN
- PHOEBUS_OPIS_LIMIT

### `PreCommit.gitlab-ci.yml`

Template to run [pre-commit](https://pre-commit.com) on all files during the `check` stage.
The project should have a `.pre-commit-config.yaml` file.

### `VagrantCloud.gitlab-ci.yml`

Defines a `release-vagrantbox` job (part of the release stage) to upload a vagrant box to Vagrant Cloud.
The job is only executed on tags.

The following variables should be defined:

- VAGRANTCLOUD_API_URL
- VAGRANTCLOUD_TOKEN
- BOX_NAME
- BOX_PATH
- BOX_VERSION

### `SonarScanner.gitlab-ci.yml`

Template to run [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/).

The variable `SONARQUBE_TOKEN` shall be defined in the CI/CD variables.
The project should include a `sonar-project.properties` file including at least the `sonar.projectKey`.

### `SphinxPages.gitlab-ci.yml`

Template to build GitLab Pages documentation with [Sphinx](https://www.sphinx-doc.org/).
The template assumes that Sphinx files are under the `docs` directory.
Documentation is only built on tag.

The variable `SPHINX_VERSION` can be set to use a specific Sphinx version.
The latest `registry.esss.lu.se/ics-docker/sphinx` docker image is used otherwise.
